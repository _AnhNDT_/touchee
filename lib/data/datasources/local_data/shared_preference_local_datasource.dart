import 'package:touchee/common/utils/database_utils.dart';
import 'package:touchee/data/models/shared_preference_model.dart';
import 'package:touchee/common/constants/shared_preference_keys_constant.dart';
import 'package:touchee/data/datasources/local_data/base_local_datasource.dart';
import 'package:touchee/common/constants/local_datasource_boxname_constants.dart';
import 'package:touchee/data/datasources/local_data/tables/shared_preference_table.dart';

class SharedPreferenceLocalDataSource
    extends BaseLocalDataSource<SharedPreferenceTable, SharedPreferenceModel> {
  SharedPreferenceLocalDataSource()
      : super(
          boxName: LocalDataSourceBoxNameConstants.sharedPreference,
        ) {
    DatabaseUtil.registerAdapter<SharedPreferenceTable>(
        SharedPreferenceTableAdapter());
  }

  @override
  Future<List<SharedPreferenceModel>> getFormattedData() async {
    final List<SharedPreferenceModel> sharedPreferences = await getAll();
    return sharedPreferences
        .map((sharedPreference) =>
            SharedPreferenceModel.fromSharedPreferenceTable(sharedPreference))
        .toList();
  }

  @override
  Future<void> insertOrUpdateItems(
      List<SharedPreferenceModel> sharedPreferenceItems) async {
    final sharedPreferenceMap = {
      for (SharedPreferenceModel item in sharedPreferenceItems)
        item.key: SharedPreferenceTable.fromModel(item)
    };
    await putAll(sharedPreferenceMap);
  }

  Future<SharedPreferenceModel> getFormattedDataByKey(String key) async {
    final sharedPreference = await get(key);
    if (sharedPreference != null) {
      return SharedPreferenceModel.fromSharedPreferenceTable(sharedPreference);
    }
    return null;
  }

  Future<void> store(String key, String value) async {
    final sharedPreferenceModel = SharedPreferenceModel(key: key, value: value);
    return put(key, SharedPreferenceTable.fromModel(sharedPreferenceModel));
  }

  Future<String> getLoginToken() async {
    return fetch(SharedPreferenceKeys.token);
  }

  Future<void> setLoginToken(String token) async {
    return store(SharedPreferenceKeys.token, token);
  }

  Future<String> getHiveToken(String boxName) async {
    return fetch(boxName);
  }

  Future<void> setHiveToken(String boxName, String token) async {
    return store(boxName, token);
  }

  Future<String> fetch(String key) async {
    final sharedPreference = await get(key);
    if (sharedPreference != null) {
      final sharedPreferenceModel =
          SharedPreferenceModel.fromSharedPreferenceTable(sharedPreference);
      return sharedPreferenceModel.value;
    }
    return '';
  }
}
