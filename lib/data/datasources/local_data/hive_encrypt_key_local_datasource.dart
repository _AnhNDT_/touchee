import 'package:touchee/data/datasources/local_data/shared_preference_local_datasource.dart';

class HiveEncryptKeyLocalDataSource {
  final SharedPreferenceLocalDataSource _sharedPreferencesHive;

  HiveEncryptKeyLocalDataSource(this._sharedPreferencesHive);

  Future<String> getHiveKey(String boxName) async {
    return _sharedPreferencesHive.getHiveToken(
      boxName,
    );
  }

  Future<void> saveHiveKey(String token, String boxName) async {
    return _sharedPreferencesHive.setHiveToken(
      boxName,
      token,
    );
  }
}
