// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shared_preference_table.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SharedPreferenceTableAdapter extends TypeAdapter<SharedPreferenceTable> {
  @override
  final typeId = 77;

  @override
  SharedPreferenceTable read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SharedPreferenceTable()
      ..key = fields[1] as String
      ..value = fields[2] as String;
  }

  @override
  void write(BinaryWriter writer, SharedPreferenceTable obj) {
    writer
      ..writeByte(2)
      ..writeByte(1)
      ..write(obj.key)
      ..writeByte(2)
      ..write(obj.value);
  }
}
