import 'package:hive/hive.dart';

import 'package:touchee/data/models/shared_preference_model.dart';
import 'package:touchee/common/constants/local_database_type_constants.dart';

part 'shared_preference_table.g.dart';

@HiveType(typeId: HiveTypeIdConstants.sharedPreferenceTableId)
class SharedPreferenceTable extends SharedPreferenceModel {
  SharedPreferenceTable({
    String key,
    String value,
  }) : super(
          key: key,
          value: value,
        );

  factory SharedPreferenceTable.fromModel(SharedPreferenceModel model) {
    return SharedPreferenceTable(
      key: model.key,
      value: model.value,
    );
  }
}
