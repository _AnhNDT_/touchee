import 'dart:io';

import 'package:flutter/material.dart';

import 'package:touchee/data/models/user_model.dart';
import 'package:touchee/common/network/http/touchee_client.dart';
import 'package:touchee/data/datasources/remote/constants/remote_constants.dart';

class UserRemoteDataSource {
  final ToucheeClient toucheeClient;

  UserRemoteDataSource({@required this.toucheeClient});

  Future<bool> changePassword({String currentPwd, String newPwd}) async {
    return false;
  }

  Future<UserModel> getUserSession({String token}) async {
    toucheeClient.updateAuthenticatedTokenHeader(token: token);
    final response = await toucheeClient.get(
      RemoteConstants.session,
      clearHttpCache: true,
    );
    return UserModel.fromJson(response);
  }

  Future<UserModel> login({String userName, String pwd}) async {
    final data = <String, dynamic>{
      'username': userName,
      'password': pwd,
    };
    final response = await toucheeClient.post(RemoteConstants.login, data);
    return UserModel.fromJson(response);
  }

  Future<void> loginMall({String mallId}) async {
    await toucheeClient.get(
      RemoteConstants.loginMall,
      params: <String, dynamic>{
        'mallId': mallId,
      },
    );
  }

  Future<bool> logout() async {
    throw UnimplementedError();
  }

  Future<void> registerPushNotification({String token, String deviceId}) async {
    final data = <String, dynamic>{
      'deviceId': deviceId,
      'deviceToken': token,
      'deviceType': Platform.operatingSystem,
    };

    await toucheeClient.post(RemoteConstants.registerPushNotification, data);
  }
}
