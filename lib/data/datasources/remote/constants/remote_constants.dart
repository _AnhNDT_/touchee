class RemoteConstants {
  static const appConfig = 'app-info';
  static const groupValue = 'group-values';
  static const rules = 'rules';

  static const campaignList = 'campaign/list';
  static const allCampaign = 'campaign/list/all';

  static const login = 'user/login';
  static const logout = 'user/logout';
  static const changePassword = 'user/change-password';
  static const session = 'user/session';
  static const registerPushNotification = 'user/push';
  static const loginMall = 'user/login-mall';

  static const saveProfile = 'profile/save';
  static const report = 'profile/list';

  static const checkSerial = 'voucher-type/check-serial';

  static const getAllMall = 'mall/list';
}
