import 'package:flutter/material.dart';

import 'package:touchee/data/models/app_config_model.dart';
import 'package:touchee/common/network/http/touchee_client.dart';
import 'package:touchee/data/datasources/remote/constants/remote_constants.dart';

class ConfigRemoteDataSource {
  final ToucheeClient toucheeClient;

  ConfigRemoteDataSource({@required this.toucheeClient});

  Future<AppConfigModel> getAppConfig() async {
    final response = await toucheeClient.get(
      RemoteConstants.appConfig,
      clearHttpCache: true,
    );

    return AppConfigModel.fromJson(response);
  }

  Future<void> registerNotification({String token, String deviceId}) async {
    final response = await toucheeClient.get(
      RemoteConstants.registerPushNotification,
      params: <String, dynamic>{
        'token': token,
        'deviceId': deviceId,
      },
    );

    debugPrint('>>>>>>> Response ${response.toString()}');
  }
}
