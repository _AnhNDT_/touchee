import 'package:touchee/common/network/network_info.dart';
import 'package:touchee/data/models/app_config_model.dart';
import 'package:touchee/domain/repositories/config_repository.dart';
import 'package:touchee/common/exceptions/network_connection_exception.dart';
import 'package:touchee/data/datasources/remote/config_remote_datasource.dart';

class ConfigRepositoryImpl extends ConfigRepository {
  final NetworkInfoImpl networkInfo;
  final ConfigRemoteDataSource configRemoteDataSource;

  ConfigRepositoryImpl(this.networkInfo, this.configRemoteDataSource);

  @override
  Future<AppConfigModel> getAppConfig() async {
    if (await networkInfo.isConnected) {
      return configRemoteDataSource.getAppConfig();
    }
    throw NetworkConnectionException();
  }

  @override
  Future<void> registerNotification({String token, String deviceId}) async {
    if (await networkInfo.isConnected) {
      await configRemoteDataSource.registerNotification();
    }
    throw NetworkConnectionException();
  }
}
