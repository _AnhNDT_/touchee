import 'package:touchee/data/models/user_model.dart';
import 'package:touchee/common/network/network_info.dart';
import 'package:touchee/domain/repositories/user_repository.dart';
import 'package:touchee/data/datasources/remote/user_remote_datasource.dart';
import 'package:touchee/common/exceptions/network_connection_exception.dart';
import 'package:touchee/data/datasources/local_data/shared_preference_local_datasource.dart';

class UserRepositoryImpl extends UserRepository {
  final NetworkInfoImpl networkInfo;
  final UserRemoteDataSource userRemoteDataSource;
  final SharedPreferenceLocalDataSource localDataSource;

  UserRepositoryImpl(
    this.networkInfo,
    this.userRemoteDataSource,
    this.localDataSource,
  );

  @override
  Future<bool> changePassword({String currentPwd, String newPwd}) async {
    if (await networkInfo.isConnected) {
      return userRemoteDataSource.changePassword(
        currentPwd: currentPwd,
        newPwd: newPwd,
      );
    }
    throw NetworkConnectionException();
  }

  @override
  Future<UserModel> getUserSession({String token}) async {
    if (await networkInfo.isConnected) {
      return userRemoteDataSource.getUserSession(token: token);
    }
    throw NetworkConnectionException();
  }

  @override
  Future<UserModel> login({String userName, String pwd}) async {
    if (await networkInfo.isConnected) {
      return userRemoteDataSource.login(userName: userName, pwd: pwd);
    }
    throw NetworkConnectionException();
  }

  @override
  Future<void> loginMall({String mallId}) async {
    if (await networkInfo.isConnected) {
      return userRemoteDataSource.loginMall(mallId: mallId);
    }
    throw NetworkConnectionException();
  }

  @override
  Future<bool> logout() async {
    if (await networkInfo.isConnected) {
      return userRemoteDataSource.logout();
    }
    throw NetworkConnectionException();
  }

  @override
  Future<void> registerPushNotification({String token, String deviceId}) async {
    if (await networkInfo.isConnected) {
      return userRemoteDataSource.registerPushNotification(
        token: token,
        deviceId: deviceId,
      );
    }
    throw NetworkConnectionException();
  }

  @override
  Future<String> getToken() async {
    return localDataSource.getLoginToken();
  }

  @override
  Future<void> setToken({String token}) {
    return localDataSource.setLoginToken(token);
  }

  @override
  Future<void> resetAllConfigs() {
    return localDataSource.deleteAll();
  }
}
