import 'package:touchee/domain/entities/app_config_entity.dart';

class AppConfigModel extends AppConfigEntity {
  AppConfigModel({
    String osName,
    String version,
    String appUrl,
    String appPolicy,
    String appAbout,
    String appCooperate,
    String appGuideUrl,
    String termsOfService,
    String userTips,
    bool forcedUpdate,
    String message,
  }) : super(
          osName: osName,
          version: version,
          appUrl: appUrl,
          appPolicy: appPolicy,
          appAbout: appAbout,
          appCooperate: appCooperate,
          appGuideUrl: appGuideUrl,
          termsOfService: termsOfService,
          userTips: userTips,
          forcedUpdate: forcedUpdate,
          message: message,
        );

  AppConfigModel.fromJson(Map<String, dynamic> json) {
    osName = json['osName'];
    version = json['version'];
    appUrl = json['appUrl'];
    appPolicy = json['appPolicy'];
    appAbout = json['appAbout'];
    appCooperate = json['appCooperate'];
    appGuideUrl = json['appGuideUrl'];
    termsOfService = json['termsOfService'];
    userTips = json['userTips'];
    forcedUpdate = json['forcedUpdate'];
    message = json['message'];
  }
}
