import 'package:touchee/domain/entities/shared_preference_entity.dart';
import 'package:touchee/data/datasources/local_data/tables/shared_preference_table.dart';

class SharedPreferenceModel extends SharedPreferenceEntity {
  SharedPreferenceModel({
    String key,
    String value,
  }) : super(
          key: key,
          value: value,
        );

  factory SharedPreferenceModel.fromJson(Map<String, dynamic> json) {
    return SharedPreferenceModel(key: json['key'], value: json['value']);
  }

  factory SharedPreferenceModel.fromSharedPreferenceTable(
          SharedPreferenceTable sharedPreference) =>
      SharedPreferenceModel(
        key: sharedPreference.key,
        value: sharedPreference.value,
      );

  static List<SharedPreferenceModel> fromListJSON(Map<String, dynamic> json) {
    try {
      final List<Object> sharedPreferences = json['data']['sharedPreferences'];
      return sharedPreferences
          .map((map) => SharedPreferenceModel.fromJson(map))
          .toList();
    } catch (err) {
      return [];
    }
  }
}
