import 'package:touchee/domain/entities/role_entity.dart';

class RoleModel extends RoleEntity {
  RoleModel({
    int id,
    String name,
  }) : super(
          id: id,
          name: name,
        );

  RoleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }
}
