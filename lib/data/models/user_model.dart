import 'package:touchee/data/models/role_model.dart';
import 'package:touchee/domain/entities/user_entity.dart';

class UserModel extends UserEntity {
  UserModel({
    int id,
    String fullName,
    String email,
    String userName,
    List<RoleModel> roles,
    String token,
  }) : super(
          id: id,
          fullName: fullName,
          email: email,
          userName: userName,
          roles: roles,
          token: token,
        );

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['fullname'];
    email = json['email'];
    userName = json['username'];
    if (json['roles'] != null) {
      roles = <RoleModel>[];
      json['roles'].forEach((v) {
        roles.add(RoleModel.fromJson(v));
      });
    }
    token = json['token'];
  }
}
