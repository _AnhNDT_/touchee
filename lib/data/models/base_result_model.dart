import 'package:touchee/domain/entities/base_result_entity.dart';

class BaseResultModel extends BaseResultEntity {
  BaseResultModel({
    int code,
    String message,
    bool isServerMessage,
    dynamic data,
  }) : super(
          code: code,
          message: message,
          data: data,
          isServerMessage: isServerMessage,
        );

  BaseResultModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    data = json['data'];
    message = json['message'];
    isServerMessage = json['isServerMessage'];
  }
}
