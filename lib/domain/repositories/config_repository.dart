import 'package:touchee/data/models/app_config_model.dart';

abstract class ConfigRepository {
  Future<AppConfigModel> getAppConfig();

  Future<void> registerNotification({String token, String deviceId});
}
