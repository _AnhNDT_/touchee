import 'package:flutter/material.dart';

import 'package:touchee/data/models/user_model.dart';

abstract class UserRepository {
  Future<UserModel> login({@required String userName, @required String pwd});

  Future<bool> logout();

  Future<UserModel> getUserSession({@required String token});

  Future<bool> changePassword({
    @required String currentPwd,
    @required String newPwd,
  });

  Future<void> registerPushNotification({
    @required String token,
    @required String deviceId,
  });

  Future<void> loginMall({@required String mallId});

  Future<String> getToken();

  Future<void> setToken({@required String token});

  Future<void> resetAllConfigs();
}
