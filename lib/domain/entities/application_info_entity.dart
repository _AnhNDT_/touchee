import 'dart:convert';

class ApplicationInfoEntity {
  final String platform;
  final String appName;
  final String packageName;
  final String version;
  final String buildNumber;
  final Map<String, dynamic> extendInfo;

  ApplicationInfoEntity({
    this.platform,
    this.appName,
    this.packageName,
    this.version,
    this.buildNumber,
    this.extendInfo,
  });

  @override
  String toString() {
    final data = <String, dynamic>{};
    data['platform'] = platform;
    data['package_name'] = packageName;
    data['version'] = version;
    data['build_number'] = buildNumber;
    data['extend_info'] = extendInfo;

    return base64.encode(utf8.encode(jsonEncode(data)));
  }
}
