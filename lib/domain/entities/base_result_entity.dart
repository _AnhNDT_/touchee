class BaseResultEntity {
  int code;
  String message;
  bool isServerMessage;
  dynamic data;

  BaseResultEntity({
    this.code,
    this.message,
    this.data,
    this.isServerMessage = false,
  });

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['code'] = code;
    data['data'] = data;
    data['message'] = message;
    data['isServerMessage'] = isServerMessage;
    return data;
  }
}
