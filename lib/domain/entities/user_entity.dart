import 'package:touchee/domain/entities/role_entity.dart';

class UserEntity {
  int id;
  String fullName;
  String email;
  String userName;
  List<RoleEntity> roles;
  String token;

  UserEntity({
    this.id,
    this.fullName,
    this.email,
    this.userName,
    this.roles,
    this.token,
  });

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['fullname'] = fullName;
    data['email'] = email;
    data['username'] = userName;
    if (roles != null) {
      data['roles'] = roles.map((v) => v.toJson()).toList();
    }
    data['token'] = token;
    return data;
  }
}
