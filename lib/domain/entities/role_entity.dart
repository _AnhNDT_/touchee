class RoleEntity {
  int id;
  String name;

  RoleEntity({this.id, this.name});

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
