import 'package:hive/hive.dart';

class SharedPreferenceEntity {
  @HiveField(1)
  String key;

  @HiveField(2)
  String value;

  SharedPreferenceEntity({
    this.key,
    this.value,
  });

  Map<String, dynamic> toJson() {
    final json = {
      'key': key,
      'value': value,
    };
    return json;
  }
}
