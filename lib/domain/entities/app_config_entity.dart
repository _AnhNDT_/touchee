class AppConfigEntity {
  String osName;
  String version;
  String appUrl;
  String appPolicy;
  String appAbout;
  String appCooperate;
  String appGuideUrl;
  String termsOfService;
  String userTips;
  bool forcedUpdate;
  String message;

  AppConfigEntity({
    this.osName,
    this.version,
    this.appUrl,
    this.appPolicy,
    this.appAbout,
    this.appCooperate,
    this.appGuideUrl,
    this.termsOfService,
    this.userTips,
    this.forcedUpdate,
    this.message,
  });

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['osName'] = osName;
    data['version'] = version;
    data['appUrl'] = appUrl;
    data['appPolicy'] = appPolicy;
    data['appAbout'] = appAbout;
    data['appCooperate'] = appCooperate;
    data['appGuideUrl'] = appGuideUrl;
    data['termsOfService'] = termsOfService;
    data['userTips'] = userTips;
    data['forcedUpdate'] = forcedUpdate;
    data['message'] = message;
    return data;
  }
}
