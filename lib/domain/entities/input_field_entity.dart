class InputFieldEntity {
  int id;
  int campaignId;
  String code;
  String name;
  bool isShow;
  bool isRequire;

  InputFieldEntity({
    this.id,
    this.campaignId,
    this.code,
    this.name,
    this.isShow,
    this.isRequire,
  });

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['campaignId'] = campaignId;
    data['code'] = code;
    data['name'] = name;
    data['isShow'] = isShow;
    data['isRequire'] = isRequire;
    return data;
  }
}
