import 'package:touchee/domain/entities/app_config_entity.dart';
import 'package:touchee/domain/repositories/config_repository.dart';

class ConfigUseCase {
  final ConfigRepository configRepository;

  ConfigUseCase({this.configRepository});

  Future<AppConfigEntity> getAppConfig() async {
    return configRepository.getAppConfig();
  }

  Future<void> registerNotification({String token, String deviceId}) async {
    return configRepository.registerNotification(
      token: token,
      deviceId: deviceId,
    );
  }
}
