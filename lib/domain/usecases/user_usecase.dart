import 'package:flutter/material.dart';

import 'package:touchee/domain/entities/user_entity.dart';
import 'package:touchee/domain/repositories/user_repository.dart';

class UserUseCase {
  final UserRepository userRepository;

  UserUseCase({this.userRepository});

  Future<UserEntity> login({String userName, String pwd}) async {
    return userRepository.login(userName: userName, pwd: pwd);
  }

  Future<bool> logout() async {
    return userRepository.logout();
  }

  Future<UserEntity> getUserSession({@required String token}) async {
    return userRepository.getUserSession(token: token);
  }

  Future<bool> changePassword({
    @required String currentPwd,
    @required String newPwd,
  }) async {
    return userRepository.changePassword(
      currentPwd: currentPwd,
      newPwd: newPwd,
    );
  }

  Future<void> registerPushNotification({
    @required String token,
    @required String deviceId,
  }) async {
    return userRepository.registerPushNotification(
      token: token,
      deviceId: deviceId,
    );
  }

  Future<void> loginMall({String mallId}) async {
    return userRepository.loginMall(mallId: mallId);
  }

  Future<String> getToken() {
    return userRepository.getToken();
  }

  Future<void> setToken({String token}) {
    return userRepository.setToken(token: token);
  }

  Future<void> resetAllConfigs() {
    return userRepository.resetAllConfigs();
  }
}
