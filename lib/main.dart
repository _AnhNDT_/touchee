import 'package:flutter/services.dart';

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:touchee/presentation/app.dart';
import 'package:touchee/common/utils/database_utils.dart';
import 'package:touchee/common/injector/injector_config.dart';
import 'package:touchee/common/configurations/configurations.dart';
import 'package:touchee/common/configurations/config_constants.dart';
import 'package:touchee/presentation/common_bloc/supervisor_bloc/supervisor_bloc.dart';

import 'env.dart' as config;

Future<void> main() async {
  InjectorConfig.setup();

  WidgetsFlutterBinding.ensureInitialized();

  Bloc.observer = MyBlocObserver();

  await DatabaseUtil.initDatabase();

  await Configuration().initConfiguration(
    flavor: FlavorEnum.dev,
    configs: config.environment,
  );

  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) => runApp(
            ToucheeApp(),
          ));
}
