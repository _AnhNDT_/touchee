part of 'loader_bloc.dart';

abstract class LoaderEvent {}

class StartLoading extends LoaderEvent {
  StartLoading();
}

class FinishLoading extends LoaderEvent {
  FinishLoading();
}
