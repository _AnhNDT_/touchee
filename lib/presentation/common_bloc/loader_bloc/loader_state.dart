part of 'loader_bloc.dart';

abstract class LoaderState {
  final bool loading;
  LoaderState({this.loading});
}

class LoadingState extends LoaderState {
  LoadingState() : super(loading: true);
}

class LoadedState extends LoaderState {
  LoadedState() : super(loading: false);
}
