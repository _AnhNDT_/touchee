import 'package:bloc/bloc.dart';

part 'loader_event.dart';
part 'loader_state.dart';

class LoaderBloc extends Bloc<LoaderEvent, LoaderState> {
  LoaderBloc() : super(LoadedState());

  @override
  Stream<LoaderState> mapEventToState(LoaderEvent event) async* {
    switch (event.runtimeType) {
      case StartLoading:
        yield LoadingState();
        break;
      case FinishLoading:
        yield LoadedState();
        break;
    }
  }
}
