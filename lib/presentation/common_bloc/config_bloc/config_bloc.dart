import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:touchee/domain/usecases/config_usecase.dart';

part 'config_event.dart';
part 'config_state.dart';

class ConfigBloc extends Bloc<ConfigEvent, ConfigState> {
  final ConfigUseCase configUseCase;

  ConfigBloc({this.configUseCase}) : super(InitConfigState());

  @override
  Stream<ConfigState> mapEventToState(ConfigEvent event) async* {
    switch (event.runtimeType) {
      case FetchConfigEvent:
        yield* _mapFetchSummaryToState(event);
        break;
      default:
        break;
    }
  }

  Stream<ConfigState> _mapFetchSummaryToState(FetchConfigEvent event) async* {
    try {} on SocketException catch (_) {
      yield NetworkExceptionState();
    } catch (_) {
      yield FailToLoadConfigState();
    }
  }
}
