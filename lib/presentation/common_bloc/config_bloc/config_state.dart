part of 'config_bloc.dart';

abstract class ConfigState {}

class InitConfigState extends ConfigState {}

class FetchConfigState extends ConfigState {}

class NetworkExceptionState extends ConfigState {}

class FailToLoadConfigState extends ConfigState {}
