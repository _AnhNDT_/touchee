part of 'config_bloc.dart';

abstract class ConfigEvent {
  bool isFromRemote;

  ConfigEvent({this.isFromRemote});
}

class FetchConfigEvent extends ConfigEvent {
  FetchConfigEvent({bool fromRemote = true}) : super(isFromRemote: fromRemote);
}
