import 'dart:io';

import 'package:flutter/material.dart';

import 'package:bloc/bloc.dart';
import 'package:pedantic/pedantic.dart';

import 'package:touchee/common/utils/toast_utils.dart';
import 'package:touchee/common/event_bus/event_bus.dart';
import 'package:touchee/common/event_bus/subscriber.dart';
import 'package:touchee/domain/usecases/user_usecase.dart';
import 'package:touchee/common/exceptions/login_exception.dart';
import 'package:touchee/presentation/common_bloc/loader_bloc/loader_bloc.dart';
import 'package:touchee/presentation/event_bus/authen/authen_emitter_events.dart';
import 'package:touchee/presentation/widgets/toast_widget/toast_widget_enum.dart';
import 'package:touchee/presentation/event_bus/authen/authen_emitter_subscriber.dart';

import 'authen_event.dart';
import 'authen_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  UserUseCase userUseCase;
  LoaderBloc loaderBloc;

  Subscriber<UnauthenticatedEmitterEvent> _subscriber;

  AuthenticationBloc({
    @required this.userUseCase,
    @required this.loaderBloc,
  }) : super(AppLoadingState()) {
    _subscriber = AuthenEmitterSubscriber(authenticationBloc: this);
    EventBus.subscribe(_subscriber);
  }

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    switch (event.runtimeType) {
      case CheckLoggedInEvent:
        yield* _mapCheckLoggedInState(event);
        break;
      case LogInEvent:
        yield* _mapLogInState(event);
        break;
      case LoggedInEvent:
        yield* _mapLoggedInState(event);
        break;
      case LoggedOutEvent:
        yield* _mapUnauthenticatedState(event);
        break;
    }
  }

  Stream<AuthenticationState> _mapCheckLoggedInState(
      CheckLoggedInEvent event) async* {
    try {
      await Future.delayed(const Duration(milliseconds: 3000));
      yield const UnAuthenticatedState();
    } on SocketException catch (_) {
      yield NetworkExceptionState();
    } catch (_) {
      unawaited(userUseCase.resetAllConfigs());
      yield const UnAuthenticatedState();
    }
  }

  Stream<AuthenticationState> _mapLogInState(LogInEvent event) async* {
    try {
      loaderBloc.add(StartLoading());
      await Future.delayed(const Duration(milliseconds: 3000));
      yield AuthenticatedState(
        token: '',
        accountInfo: null,
      );
    } on DataException catch (ex) {
      ToastUtils.show(
        message: ex?.data?.message ?? '',
        translateKey: 'common.message.unknownError',
        type: ToastEnum.warning,
      );
    } on SocketException catch (_) {
      ToastUtils.show(
        translateKey: 'common.message.checkConnection',
        type: ToastEnum.warning,
      );
    } catch (e) {
      ToastUtils.show(
        translateKey: 'common.message.unknownError',
        type: ToastEnum.error,
      );
    } finally {
      loaderBloc.add(FinishLoading());
    }
  }

  Stream<AuthenticationState> _mapLoggedInState(LoggedInEvent event) async* {
    try {} catch (_) {
      yield const UnAuthenticatedState();
    }
  }

  Stream<AuthenticationState> _mapUnauthenticatedState(
      AuthenticationEvent event) async* {
    unawaited(userUseCase.resetAllConfigs());
    yield const UnAuthenticatedState();
  }
}
