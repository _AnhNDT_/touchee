import 'package:flutter/material.dart';

import 'package:touchee/domain/entities/user_entity.dart';

abstract class AuthenticationState {
  final String token;
  final UserEntity accountInfo;

  const AuthenticationState({this.token, this.accountInfo});

  List<Object> get props => [token ?? '', accountInfo];
}

class Uninitialized extends AuthenticationState {
  Uninitialized() : super(token: null, accountInfo: null);
}

class AuthenticatedState extends AuthenticationState {
  const AuthenticatedState({
    @required String token,
    @required UserEntity accountInfo,
  })  : assert(token != null || accountInfo != null,
            'token or account info cannot be null'),
        super(token: token, accountInfo: accountInfo);
}

class UnAuthenticatedState extends AuthenticationState {
  const UnAuthenticatedState() : super(token: null, accountInfo: null);
}

class NotGrantStoreState extends AuthenticationState {
  const NotGrantStoreState({
    @required String token,
    @required UserEntity accountInfo,
  })  : assert(token != null || accountInfo != null,
            'token or account info cannot be null'),
        super(token: token, accountInfo: accountInfo);
}

class AppLoadingState extends AuthenticationState {}

class NetworkExceptionState extends AuthenticationState {}
