import 'package:flutter/material.dart';

import 'package:touchee/domain/entities/user_entity.dart';

abstract class AuthenticationEvent {}

class LoggedInEvent extends AuthenticationEvent {
  final UserEntity accountInfo;

  LoggedInEvent({
    @required this.accountInfo,
  }) : assert(accountInfo != null, 'AccountInfo cannot be null');
}

class CheckLoggedInEvent extends AuthenticationEvent {}

class LogInEvent extends AuthenticationEvent {
  final String userName;
  final String password;

  LogInEvent({@required this.userName, @required this.password});
}

class LoggedOutEvent extends AuthenticationEvent {
  LoggedOutEvent();
}
