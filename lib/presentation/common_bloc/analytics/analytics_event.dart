abstract class AnalyticsEvent {}

class ScreenTrackingEvent extends AnalyticsEvent {}

class ActionTrackingEvent extends AnalyticsEvent {}

class ChangeHomeTabEvent extends AnalyticsEvent {
  final int position;

  ChangeHomeTabEvent({this.position});
}

class SeeBundleEvent extends AnalyticsEvent {
  final String bundleId;
  final String bundleName;

  SeeBundleEvent({
    this.bundleId,
    this.bundleName,
  });
}

class PurchaseBundleEvent extends SeeBundleEvent {
  final String bundlePrice;

  PurchaseBundleEvent({this.bundlePrice}) : super();
}
