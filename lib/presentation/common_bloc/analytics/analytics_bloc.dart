import 'package:flutter_bloc/flutter_bloc.dart';

import 'analytics_event.dart';
import 'analytics_state.dart';

class AnalyticsBloc extends Bloc<AnalyticsEvent, AnalyticsState> {
  AnalyticsBloc() : super(InitAnalyticsState());

//  final FirebaseAnalytics analytics;
//
//  AnalyticsBloc({@required this.analytics});

  @override
  Stream<AnalyticsState> mapEventToState(AnalyticsEvent event) async* {
    switch (event.runtimeType) {
      case ScreenTrackingEvent:
        yield* _doTrackingScreen();
        break;
      case ActionTrackingEvent:
        yield* _doActionTracking();
        break;
      case ChangeHomeTabEvent:
        yield* _doChangeHomeTabTracking(event);
        break;
    }
  }

  Stream<AnalyticsState> _doTrackingScreen() async* {}

  Stream<AnalyticsState> _doActionTracking() async* {}

  Stream<AnalyticsState> _doChangeHomeTabTracking(
      ChangeHomeTabEvent event) async* {
//    await analytics?.logEvent(
//      name: AnalyticsConstants.changeTab,
//      parameters: {'int': event.position},
//    );
  }
}
