import 'package:touchee/common/notifier/base_notifier.dart';
import 'package:touchee/common/utils/validation_utils.dart';
import 'package:touchee/presentation/journey/login/login_form_controller.dart';

class LoginFormNotifier extends BaseValueChangeNotifier<LoginFormController> {
  LoginFormNotifier(LoginFormController value) : super(value) {
    value.userNameController.addListener(() {
      final userName = value.userNameController.text.trim();

      if (!ValidationUtils.isValidUserName(userName.trim())) {
        value.userNameErrorMessage = 'validation.userName.errorMessage';
      } else {
        value.userNameErrorMessage = null;
      }

      notifyListeners();
    });

    value.passwordController.addListener(() {
      final password = value.passwordController.text.trim();

      if (password.length < 5) {
        value.passwordErrorMessage = 'validation.password.errorMessage';
      } else {
        value.passwordErrorMessage = null;
      }

      notifyListeners();
    });
  }

  LoginFormController get controller => value;

  @override
  void dispose() {
    value.userNameController.dispose();
    value.passwordController.dispose();

    value.userNameFocusNode.dispose();
    value.passwordFocusNode.dispose();

    super.dispose();
  }
}
