import 'package:flutter/material.dart';

class LoginFormController {
  String userNameErrorMessage;
  String passwordErrorMessage;

  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final FocusNode userNameFocusNode = FocusNode();
  final FocusNode passwordFocusNode = FocusNode();

  bool isValid() {
    if (userNameErrorMessage != null || userNameController.text.isEmpty) {
      return false;
    }

    if (passwordErrorMessage != null || passwordController.text.isEmpty) {
      return false;
    }

    return true;
  }
}
