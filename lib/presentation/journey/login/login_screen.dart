import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:touchee/common/injector/injector.dart';
import 'package:touchee/common/utils/screen_utils.dart';
import 'package:touchee/common/internationalization.dart';
import 'package:touchee/common/constants/router_constants.dart';
import 'package:touchee/common/constants/graphic_constants.dart';
import 'package:touchee/presentation/journey/base/base_state_widget.dart';
import 'package:touchee/presentation/journey/login/login_form_notifier.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_event.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_state.dart';
import 'package:touchee/presentation/journey/login/login_form_controller.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_bloc.dart';
import 'package:touchee/presentation/widgets/button_widget/button_widget.dart';
import 'package:touchee/presentation/widgets/spacing_widget/spacing_widget.dart';
import 'package:touchee/presentation/widgets/custom_textfield/custom_textfield.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends BaseStateWidget<LoginScreen> {
  // ignore: close_sinks
  AuthenticationBloc _authenticationBloc;
  LoginFormNotifier _notifier;

  @override
  void initState() {
    super.initState();
    if (!mounted) {
      return;
    }

    _authenticationBloc = Injector.resolve<AuthenticationBloc>();
    final _loginFormController = LoginFormController();
    _notifier = LoginFormNotifier(_loginFormController);
  }

  @override
  void dispose() {
    _notifier?.dispose();
    super.dispose();
  }

  @override
  Widget buildContentView(BuildContext context) {
    return BlocListener(
      cubit: _authenticationBloc,
      listener: (context, state) {
        switch (state.runtimeType) {
          case AuthenticatedState:
            Navigator.of(context).pushNamedAndRemoveUntil(
              RouteName.homeScreen,
              ModalRoute.withName(RouteName.initial),
            );
            break;
          default:
            break;
        }
      },
      child: ValueListenableBuilder(
        valueListenable: _notifier,
        builder: (context, LoginFormController controller, child) {
          return _buildContentWidget(context, controller);
        },
      ),
    );
  }

  Widget _buildContentWidget(
      BuildContext context, LoginFormController controller) {
    final _size = MediaQuery.of(context).size;
    final i18n = S.of(context);
    return SingleChildScrollView(
      child: Container(
        height: _size.height,
        child: Center(
          child: Container(
            width: ScreenUtils().getFormWidth(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: Image.asset(
                    GraphicConstants.icLogo,
                    width: _size.width * 0.45,
                  ),
                ),

                /// Username
                CustomTextField(
                  textController: controller.userNameController,
                  focusNode: controller.userNameFocusNode,
                  hint: i18n.translate('loginScreen.hint.userName'),
                  prefixIcon: const Icon(Icons.account_circle),
                  inputAction: TextInputAction.next,
                  errorText: i18n.translate(controller.userNameErrorMessage),
                  onSubmitted: (_text) {
                    FocusScope.of(context).requestFocus(
                      controller.passwordFocusNode,
                    );
                  },
                ),
                SpacingWidget(verticalSpacing: 20),

                /// Password
                CustomTextField(
                  textController: controller.passwordController,
                  focusNode: controller.passwordFocusNode,
                  hint: i18n.translate('loginScreen.hint.password'),
                  prefixIcon: const Icon(Icons.lock),
                  errorText: i18n.translate(controller.passwordErrorMessage),
                  isPassword: true,
                  inputAction: TextInputAction.done,
                  onSubmitted: (_text) {
                    if (controller.isValid()) {
                      _doLogin(controller);
                    }
                  },
                ),
                SpacingWidget(verticalSpacing: 50),
                Container(
                  width: double.infinity,
                  height: 45,
                  child: ButtonWidget.primary(
                    title: i18n.translate('common.button.login'),
                    onPress: controller.isValid()
                        ? () {
                            _doLogin(controller);
                          }
                        : null,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _doLogin(LoginFormController controller) {
    _authenticationBloc.add(
      LogInEvent(
        userName: controller.userNameController.text.trim(),
        password: controller.passwordController.text.trim(),
      ),
    );
  }
}
