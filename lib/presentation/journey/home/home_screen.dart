import 'package:flutter/material.dart';

import 'package:touchee/common/utils/toast_utils.dart';
import 'package:touchee/common/constants/logger_constants.dart';
import 'package:touchee/presentation/journey/base/base_state_widget.dart';
import 'package:touchee/presentation/widgets/button_widget/button_widget.dart';
import 'package:touchee/presentation/widgets/spacing_widget/spacing_widget.dart';
import 'package:touchee/presentation/widgets/toast_widget/toast_widget_enum.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends BaseStateWidget<HomeScreen> {
  @override
  Widget buildContentView(BuildContext context) {
    toucheeLog.e('Hello colorful logger');
    toucheeLog.d('Hello colorful logger');
    toucheeLog.w('Hello colorful logger');
    toucheeLog.i('Hello colorful logger');

    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Hello world, Touchee'),
          SpacingWidget(verticalSpacing: 20),
          ButtonWidget.primary(
            title: 'Show info ',
            onPress: () {
              ToastUtils.show(
                message: 'Hello Touchee',
                type: ToastEnum.info,
              );
            },
          ),
          SpacingWidget(verticalSpacing: 10),
          ButtonWidget.primary(
            title: 'Show error',
            onPress: () {
              ToastUtils.show(
                message: 'Hello Touchee',
                type: ToastEnum.error,
              );
            },
          ),
          SpacingWidget(verticalSpacing: 10),
          ButtonWidget.primary(
            title: 'Show warning',
            onPress: () {
              ToastUtils.show(
                message: 'Hello Touchee',
                type: ToastEnum.warning,
              );
            },
          ),
          SpacingWidget(verticalSpacing: 10),
          ButtonWidget.secondary(
            title: 'Show info',
            onPress: () {
              ToastUtils.show(
                message: 'Hello Touchee',
              );
            },
          ),
        ],
      ),
    );
  }
}
