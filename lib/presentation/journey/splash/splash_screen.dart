import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:touchee/common/injector/injector.dart';
import 'package:touchee/common/internationalization.dart';
import 'package:touchee/presentation/theme/theme_color.dart';
import 'package:touchee/common/constants/router_constants.dart';
import 'package:touchee/common/constants/graphic_constants.dart';
import 'package:touchee/presentation/widgets/modal_widget/modal.dart';
import 'package:touchee/presentation/journey/base/base_state_widget.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_bloc.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_event.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_state.dart';
import 'package:touchee/presentation/widgets/button_widget/flat_button_widget.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends BaseStateWidget<SplashScreen> {
  // ignore: close_sinks
  AuthenticationBloc authenticationBloc;

  @override
  void initState() {
    super.initState();
    if (!mounted) {
      return;
    }

    authenticationBloc = Injector.resolve<AuthenticationBloc>()
      ..add(CheckLoggedInEvent());
  }

  @override
  Widget buildContentView(BuildContext context) {
    return Container(
      child: Stack(
        fit: StackFit.expand,
        children: [
          Container(
            color: AppColor.kColorWhite,
            child: Center(
              child: Image.asset(
                GraphicConstants.icLogo,
                width: MediaQuery.of(context).size.width * 0.45,
              ),
            ),
          ),
          Center(
            child: BlocListener<AuthenticationBloc, AuthenticationState>(
              cubit: authenticationBloc,
              listener: (context, state) {
                switch (state.runtimeType) {
                  case AuthenticatedState:
                    Navigator.of(context).pushNamedAndRemoveUntil(
                      RouteName.homeScreen,
                      ModalRoute.withName(RouteName.initial),
                    );
                    break;
                  case UnAuthenticatedState:
                    Navigator.of(context).pushNamedAndRemoveUntil(
                      RouteName.loginScreen,
                      ModalRoute.withName(RouteName.initial),
                    );
                    break;
                  case NetworkExceptionState:
                    _buildNoNetworkDialogModel(context);
                    break;
                  default:
                    break;
                }
              },
              child: Column(
                children: <Widget>[
                  Expanded(child: Container()),
                  Text(
                    S.of(context).translate('splashScreen.message.delayTime'),
                    style: Theme.of(context).textTheme.headline6.copyWith(
                          color: AppColor.black80,
                          fontSize: 16,
                        ),
                  ),
                  const SizedBox(height: 20)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Future _buildNoNetworkDialogModel(BuildContext context) {
    final i18n = S.of(context).translate;
    return Modal(
      isFixedHorizontalActions: true,
      title: i18n('common.dialog.networkErrorTitle'),
      description: i18n('common.dialog.networkErrorDescription'),
      context: context,
      actions: [
        Expanded(child: Container()),
        SizedBox(
          height: 40,
          child: FlatButtonWidget.dialogPositive(
            title: i18n('common.button.retry'),
            onPress: () {
              Navigator.of(context).pop();
              authenticationBloc.add(CheckLoggedInEvent());
            },
          ),
        ),
      ],
    ).showModal();
  }
}
