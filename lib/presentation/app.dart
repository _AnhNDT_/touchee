import 'package:flutter/material.dart';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:touchee/presentation/routes.dart';
import 'package:touchee/common/injector/injector.dart';
import 'package:touchee/common/internationalization.dart';
import 'package:touchee/presentation/theme/theme_app.dart';
import 'package:touchee/presentation/theme/theme_color.dart';
import 'package:touchee/common/constants/router_constants.dart';
import 'package:touchee/common/constants/language_constants.dart';
import 'package:touchee/presentation/widgets/loader_widget/loader_widget.dart';

import 'common_bloc/analytics/analytics_bloc.dart';
import 'common_bloc/authen_bloc/authen_bloc.dart';
import 'common_bloc/language_bloc/language_bloc.dart';
import 'common_bloc/loader_bloc/loader_bloc.dart';

class ToucheeApp extends StatelessWidget {
  final _navigator = GlobalKey<NavigatorState>();
  final _botToastBuilder = BotToastInit();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.kPrimaryColor,
      child: MultiBlocProvider(
        providers: _getProviders(),
        child: BlocBuilder<LanguageBloc, LanguageState>(
          builder: (context, state) {
            return MaterialApp(
              title: 'Touchee',
              initialRoute: RouteName.initial,
              onGenerateRoute: Routes.generateRoute,
              theme: AppTheme().build(context, brightness: Brightness.light),
              localizationsDelegates: [
                SLocalizationsDelegate(),
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              localeResolutionCallback: (locale, supportedLocales) {
                if (locale == null) {
                  return supportedLocales.first;
                }

                for (final supportedLocale in supportedLocales) {
                  if (supportedLocale.languageCode == locale.languageCode &&
                      supportedLocale.countryCode == locale.countryCode) {
                    return supportedLocale;
                  }
                }
                return supportedLocales.first;
              },
              supportedLocales: const [
                Locale(LanguageConstants.vietnamese, 'VN'),
              ],
              builder: (context, child) {
                child = LoadingContainer(
                  navigator: _navigator,
                  child: child,
                );

                return _botToastBuilder(context, child);
              },
              navigatorObservers: [BotToastNavigatorObserver()],
            );
          },
        ),
      ),
    );
  }

  List<BlocProvider> _getProviders() => [
        BlocProvider<LanguageBloc>(
            create: (BuildContext context) => Injector.resolve<LanguageBloc>()),
        BlocProvider<LoaderBloc>(
            create: (BuildContext context) => Injector.resolve<LoaderBloc>()),
        BlocProvider<AnalyticsBloc>(
            create: (BuildContext context) =>
                Injector.resolve<AnalyticsBloc>()),
        BlocProvider<AuthenticationBloc>(
            create: (BuildContext context) =>
                Injector.resolve<AuthenticationBloc>()),
      ];
}
