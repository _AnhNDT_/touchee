import 'package:flutter/material.dart';

class CustomDropdownButton<T> extends StatelessWidget {
  final List<DropdownMenuItem<T>> items;
  final Object value;
  final String hint;
  final String label;
  final ValueChanged<T> onChanged;
  final InputDecoration decoration;

  CustomDropdownButton(
      {this.items,
      this.value,
      this.hint,
      this.label,
      this.decoration,
      this.onChanged});

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      decoration: decoration ??
          InputDecoration(
            labelText: label ?? '',
            hintMaxLines: 1,
            contentPadding: EdgeInsets.symmetric(
                horizontal: 15, vertical: label?.isEmpty == true ? 0 : 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context).unselectedWidgetColor, width: 0.5),
            ),
          ),
      isExpanded: true,
      items: items,
      hint: Text('${hint ?? ''}'),
      value: value ?? '',
      onChanged: (_value) {
        if (onChanged != null) {
          onChanged(_value);
        }
      },
      style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 16),
    );
  }
}
