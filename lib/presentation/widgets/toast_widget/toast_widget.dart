import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:touchee/common/internationalization.dart';
import 'package:touchee/presentation/theme/theme_color.dart';
import 'package:touchee/presentation/widgets/blank_widget/blank_widget.dart';
import 'package:touchee/presentation/widgets/toast_widget/toast_widget_enum.dart';

class ToastWidget extends StatelessWidget {
  final String message;
  final String translateKey;
  final ToastEnum type;

  ToastWidget({
    this.message = '',
    this.translateKey,
    this.type = ToastEnum.info,
  });

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case ToastEnum.error:
        return _buildInfoMessageWidget(
          context,
          AppColor.kColorToastError.withOpacity(.5),
        );
      case ToastEnum.warning:
        return _buildInfoMessageWidget(
          context,
          AppColor.kColorToastWarning.withOpacity(.5),
        );
      default:
        return _buildInfoMessageWidget(
          context,
          AppColor.kColorToastInfo.withOpacity(.1),
        );
    }
  }

  Widget _buildInfoMessageWidget(BuildContext context, Color color) {
    var messageValue = '';
    if (message?.isNotEmpty ?? false) {
      messageValue = message;
    } else if (translateKey?.isNotEmpty ?? false) {
      messageValue = S.of(context).translate(translateKey);
    }

    if (messageValue.isEmpty) {
      return BlankWidget();
    }

    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 20,
      ),
      width: ScreenUtil.screenWidth * .975,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: color.withOpacity(.9),
        boxShadow: const [
          BoxShadow(
            blurRadius: 5,
            spreadRadius: 0,
            color: AppColor.shadowColor,
            offset: Offset(0, 2),
          )
        ],
      ),
      child: Text(
        messageValue,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyText2.copyWith(
              color: AppColor.kColorWhite,
            ),
      ),
    );
  }
}
