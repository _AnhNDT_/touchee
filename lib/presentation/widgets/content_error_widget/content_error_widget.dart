import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:touchee/common/internationalization.dart';
import 'package:touchee/presentation/theme/theme_color.dart';
import 'package:touchee/common/constants/graphic_constants.dart';

import 'content_error_enum.dart';

class ContentErrorWidget extends StatelessWidget {
  final ContentErrorEnum type;

  ContentErrorWidget({this.type = ContentErrorEnum.commonError});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          width: ScreenUtil.screenWidth * .5,
          height: ScreenUtil.screenWidth * .5,
          child: Image.asset(_getImagePath()),
        ),
        SizedBox(height: 20.h),
        Text(
          _getMessage(context),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1.copyWith(
                fontSize: 16,
                color: AppColor.blackOpacity5,
              ),
        ),
        SizedBox(height: 50.h),
      ],
    );
  }

  String _getImagePath() {
    switch (type) {
      case ContentErrorEnum.noSignal:
        return GraphicConstants.icNoSignal;
      case ContentErrorEnum.notGrantPermission:
        return GraphicConstants.icNotGrantPermission;
      default:
        return GraphicConstants.icError;
    }
  }

  String _getMessage(BuildContext context) {
    final i18n = S.of(context).translate;
    switch (type) {
      case ContentErrorEnum.noSignal:
        return i18n('common.message.connectionError');
      case ContentErrorEnum.notGrantPermission:
        return i18n('common.message.notGrantPermission');
      default:
        return i18n('common.message.UnknownError');
    }
  }
}
