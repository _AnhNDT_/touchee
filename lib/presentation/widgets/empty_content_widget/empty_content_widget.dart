import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:touchee/common/internationalization.dart';
import 'package:touchee/presentation/theme/theme_color.dart';
import 'package:touchee/common/constants/graphic_constants.dart';

class EmptyContentWidget extends StatelessWidget {
  final String message;

  EmptyContentWidget({this.message});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: ScreenUtil.screenWidth * .5,
            height: ScreenUtil.screenWidth * .5,
            child: Image.asset(GraphicConstants.icNoData),
          ),
          SizedBox(height: 20.h),
          Text(
            message ?? S.of(context).translate('common.message.emptyContent'),
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  fontSize: 16,
                  color: AppColor.blackOpacity5,
                ),
          ),
          SizedBox(height: 50.h),
        ],
      ),
    );
  }
}
