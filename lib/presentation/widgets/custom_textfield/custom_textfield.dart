import 'package:flutter/material.dart';

import 'package:touchee/presentation/theme/theme_color.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController textController;
  final String hint;
  final Icon prefixIcon;
  final Widget suffixWidget;
  final bool isPassword;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmitted;
  final String errorText;
  final TextInputAction inputAction;
  final FocusNode focusNode;
  final int errorMaxLines;
  final InputBorder focusBorder;
  final InputBorder enabledBorder;
  final InputBorder errorBorder;
  final TextInputType textInputType;
  final int maxLength;
  final bool isShowCounterText;
  final TextCapitalization textCapitalization;
  final bool autoFocus;

  CustomTextField({
    this.textController,
    this.hint,
    this.prefixIcon,
    this.suffixWidget,
    this.isPassword = false,
    this.onChanged,
    this.onSubmitted,
    this.errorText,
    this.inputAction,
    this.focusNode,
    this.errorMaxLines = 10,
    this.errorBorder,
    this.focusBorder,
    this.enabledBorder,
    this.textInputType,
    this.maxLength,
    this.isShowCounterText = false,
    this.textCapitalization = TextCapitalization.none,
    this.autoFocus = false,
  });

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    return TextField(
      controller: textController,
      obscureText: isPassword,
      focusNode: focusNode,
      maxLength: maxLength,
      textCapitalization: textCapitalization,
      autofocus: autoFocus,
      decoration: InputDecoration(
        counterText: isShowCounterText ? null : '',
        prefixIcon: prefixIcon,
        suffixIcon: suffixWidget,
        hintText: hint,
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: _theme.unselectedWidgetColor,
            width: 0.5,
          ),
        ),
        focusedBorder: focusBorder ??
            OutlineInputBorder(
              borderSide: BorderSide(
                color: _theme.unselectedWidgetColor,
                width: 0.5,
              ),
            ),
        enabledBorder: enabledBorder ??
            OutlineInputBorder(
              borderSide: BorderSide(
                color: _theme.unselectedWidgetColor,
                width: 0.5,
              ),
            ),
        errorBorder: errorBorder ??
            const OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColor.kAccentColor,
                width: 1.5,
              ),
            ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: _theme.primaryColor,
            width: 0.68,
          ),
        ),
        errorText: errorText?.isNotEmpty == true ? errorText : null,
        errorStyle: _theme.textTheme.caption.copyWith(color: _theme.errorColor),
        errorMaxLines: errorMaxLines,
        hintStyle: _theme.textTheme.subtitle2.copyWith(
          color: AppColor.kColorGray,
        ),
      ),
      keyboardType: textInputType ?? TextInputType.text,
      textInputAction: inputAction ?? TextInputAction.done,
      onChanged: onChanged,
      onSubmitted: onSubmitted,
    );
  }
}
