import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:touchee/common/internationalization.dart';
import 'package:touchee/presentation/theme/theme_color.dart';

class ExpandableWidget extends StatefulWidget {
  final bool isCollapsed;
  final Widget child;
  final Function(bool) onChanged;

  const ExpandableWidget({
    Key key,
    this.isCollapsed = false,
    this.child,
    this.onChanged,
  }) : super(key: key);

  @override
  _ExpandableWidgetState createState() => _ExpandableWidgetState();
}

class _ExpandableWidgetState extends State<ExpandableWidget>
    with TickerProviderStateMixin {
  AnimationController expandAnimationController;
  AnimationController rotateAnimationController;

  Animation<double> expandAnimation;
  Animation<double> rotateAnimation;

  @override
  void initState() {
    super.initState();
    expandAnimationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    rotateAnimationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    expandAnimation = CurvedAnimation(
      parent: expandAnimationController,
      curve: Curves.fastOutSlowIn,
    );

    rotateAnimation =
        Tween(begin: 0.0, end: 0.5).animate(rotateAnimationController);
  }

  @override
  void dispose() {
    expandAnimationController?.dispose();
    rotateAnimationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isCollapsed) {
      expandAnimationController.reverse();
      rotateAnimationController.reverse();
    } else {
      expandAnimationController.forward();
      rotateAnimationController.forward();
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        AnimatedOpacity(
          opacity: widget.isCollapsed ? 0 : 1,
          duration: const Duration(milliseconds: 500),
          child: SizeTransition(
            axisAlignment: 1.0,
            sizeFactor: expandAnimation,
            child: widget.child,
          ),
        ),
        InkWell(
          onTap: () {
            _toggle();
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: AnimatedBuilder(
                      animation: rotateAnimation,
                      builder: (_, __) {
                        return Text(
                          S.of(context).translate(
                                (expandAnimationController?.isCompleted ??
                                        false)
                                    ? 'reportScreen.text.hideFilter'
                                    : 'reportScreen.text.showFilter',
                              ),
                          maxLines: 1,
                          textAlign: TextAlign.end,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.bodyText1.copyWith(
                                color: AppColor.kPrimaryColor,
                              ),
                        );
                      },
                    ),
                  ),
                ),
                RotationTransition(
                  turns: rotateAnimation,
                  child: const Icon(
                    Icons.arrow_drop_down,
                    color: AppColor.kPrimaryColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void _toggle() {
    if (expandAnimationController.isAnimating) {
      return;
    }

    if (expandAnimationController?.isCompleted ?? false) {
      expandAnimationController.reverse();
      rotateAnimationController.reverse();
      if (widget.onChanged != null) {
        widget.onChanged(true);
      }
    } else {
      expandAnimationController.forward();
      rotateAnimationController.forward();
      if (widget.onChanged != null) {
        widget.onChanged(false);
      }
    }
  }
}
