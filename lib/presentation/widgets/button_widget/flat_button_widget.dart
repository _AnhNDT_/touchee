import 'package:flutter/material.dart';

import 'package:touchee/presentation/theme/theme_text.dart';
import 'package:touchee/presentation/theme/theme_color.dart';

class FlatButtonWidget extends FlatButton {
  final Function onPress;
  final String title;
  final TextStyle activeStyle;
  final TextStyle inactiveStyle;
  @override
  final Color color;

  FlatButtonWidget.dialogPositive({
    Key key,
    this.onPress,
    this.title,
    this.activeStyle,
    this.inactiveStyle,
    this.color = AppColor.kColorWhite,
  }) : super(
          key: key,
          onPressed: onPress,
          splashColor: AppColor.kPrimaryColor.withOpacity(.1),
          child: Text(
            title,
            style:
                ThemeText.getDefaultTextTheme().textPositiveDialogButtonStyle,
          ),
          color: color,
          disabledColor: AppColor.athensGray,
        );

  FlatButtonWidget.dialogNegative({
    Key key,
    this.onPress,
    this.title,
    this.activeStyle,
    this.inactiveStyle,
    this.color = AppColor.kColorWhite,
  }) : super(
          key: key,
          onPressed: onPress,
          splashColor: AppColor.kPrimaryColor.withOpacity(.1),
          child: Text(
            title,
            style:
                ThemeText.getDefaultTextTheme().textNegativeDialogButtonStyle,
          ),
          color: color,
          disabledColor: AppColor.athensGray,
        );
}
