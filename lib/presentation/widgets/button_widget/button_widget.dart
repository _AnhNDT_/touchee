import 'package:flutter/material.dart';

import 'package:touchee/presentation/theme/theme_text.dart';
import 'package:touchee/presentation/theme/theme_color.dart';

class ButtonWidget extends RaisedButton {
  final Function onPress;
  final String title;
  final TextStyle activeStyle;
  final TextStyle inactiveStyle;
  @override
  final Color color;

  ButtonWidget.primary({
    Key key,
    this.onPress,
    this.title,
    this.activeStyle,
    this.inactiveStyle,
    this.color = AppColor.kPrimaryColor,
  }) : super(
          key: key,
          elevation: 0,
          onPressed: onPress,
          child: Text(title,
              style: onPress != null
                  ? activeStyle ??
                      ThemeText.getDefaultTextTheme().textButtonStyle
                  : inactiveStyle ??
                      ThemeText.getDefaultTextTheme().textButtonDisableStyle),
          color: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          disabledColor: AppColor.athensGray,
        );

  ButtonWidget.secondary({
    Key key,
    this.onPress,
    this.title,
    this.activeStyle,
    this.inactiveStyle,
    this.color = AppColor.kColorWhite,
  }) : super(
          key: key,
          elevation: 0,
          onPressed: onPress,
          splashColor: AppColor.kPrimaryColor.withOpacity(.5),
          child: Text(title,
              style: onPress != null
                  ? activeStyle ??
                      ThemeText.getDefaultTextTheme()
                          .textButtonStyle
                          .copyWith(color: AppColor.kPrimaryColor)
                  : inactiveStyle ??
                      ThemeText.getDefaultTextTheme().textButtonDisableStyle),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: BorderSide(
              width: 1.5,
              color: onPress != null
                  ? AppColor.kPrimaryColor
                  : AppColor.kColorBlack.withOpacity(.1),
              style: BorderStyle.solid,
            ),
          ),
          color: color,
          disabledColor: AppColor.athensGray,
        );
}
