import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:touchee/presentation/theme/theme_color.dart';

class ThemeText {
  static const display1 = TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.normal,
  );

  static const display2 = TextStyle(
    fontSize: 26,
    fontWeight: FontWeight.normal,
  );

  static const display3 =
      TextStyle(fontSize: 32, fontWeight: FontWeight.normal);

  static const display4 = TextStyle(fontSize: 38, fontWeight: FontWeight.bold);

  static const TextStyle body1 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.normal,
  );

  static const TextStyle body2 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.normal,
  );

  static const title = TextStyle(
    fontSize: 28,
    fontWeight: FontWeight.bold,
  );

  static const subtitle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.normal,
    color: AppColor.dustyGray,
  );

  static const TextStyle subhead = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.normal,
  );

  static const headline = TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.normal,
  );

  static const caption = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.normal,
  );

  static const overline = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.4,
  );

  static const button = TextStyle(
    fontSize: 19,
    fontWeight: FontWeight.normal,
  );

  static const sectionHeader = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w600,
    color: Colors.redAccent,
  );

  static final TextStyle rubberSubhead = TextStyle(
    fontSize: 17,
    color: Colors.black.withOpacity(0.4),
    letterSpacing: 0.0,
    fontWeight: FontWeight.w600,
  );

  static TextTheme getDefaultTextTheme() => const TextTheme(
        headline1: ThemeText.body1,
        headline2: ThemeText.display4,
        headline3: ThemeText.display3,
        headline4: ThemeText.display2,
        headline5: ThemeText.display1,
        headline6: ThemeText.headline,
        bodyText1: ThemeText.subtitle,
        bodyText2: ThemeText.body2,
        subtitle1: ThemeText.body1,
        subtitle2: ThemeText.subhead,
        caption: ThemeText.caption,
        overline: ThemeText.overline,
        button: ThemeText.button,
      );

  static final TextStyle defaultTitle = TextStyle(
    fontSize: ScreenUtil().setSp(48.0),
    fontWeight: FontWeight.bold,
  );

  static final TextStyle defaultSubtitle = TextStyle(
    fontSize: ScreenUtil().setSp(32.0),
  );
}

extension CustomTextTheme on TextTheme {
  TextStyle get textButtonStyle => const TextStyle(
        color: AppColor.kColorWhite,
        fontSize: 16.0,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
      );

  TextStyle get textButtonDisableStyle => TextStyle(
        color: AppColor.black25,
        fontSize: 16.0,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
      );

  TextStyle get textPositiveDialogButtonStyle => TextStyle(
        color: AppColor.kPrimaryColor,
        fontSize: ScreenUtil().setSp(32.0),
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
      );

  TextStyle get textNegativeDialogButtonStyle => TextStyle(
        color: AppColor.kColorBlue,
        fontSize: ScreenUtil().setSp(32.0),
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
      );
}
