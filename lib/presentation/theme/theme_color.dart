import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AppColor {
  static const kPrimaryColor = Color(0xFFeebb4d);
  static const kAccentColor = Color(0xFFe8505b);
  static const kSecondaryColor = Color(0xFF14b1ab);
  static const kIconColor = Color(0xFF07689f);

  static const kColorPurple = Color(0xFFb41f8e);
  static const kColorPurpleDark = Color(0xFF88186c);
  static const kColorPurpleLight = Color(0xFFdb2bae);

  static const kColorOrange = Color(0xFFff8a65);
  static const kColorOrangeDark = Color(0xFFc75b39);
  static const kColorRed = Color(0xFFE81C4F);

  static const kColorToastInfo = Color(0xFF0f4c75);
  static const kColorToastWarning = Color(0xFFFF9800);
  static const kColorToastError = Color(0xFFF44234);

  static const kColorBlueBare = Color(0xFFDBF1Fa);
  static const kColorLightGray = Color(0xFFF0F0F3);

  static const kColorTransparent = Color(0x00000000);
  static const kColorGrayBareA1 = Color(0x50E1E2E8);

  static const kColorYellowLightA1 = Color(0x50FFFF8D);

  static const kColorGray = Color(0xFF979797);
  static const kColorGrayDark = Color(0xFF606060);
  static const kColorGrayLight = Color(0xFFdedede);
  static const kColorGrayLighter = Color(0xFFF6F7F9);
  static const kColorGrayBare = Color(0xFFE1E2E8);

  static const kColorBlack = Color(0xFF000000);
  static const kColorBlackLight = Color(0xDD000000);

  static const kColorWhite = Color(0xFFffffff);

  static const kColorPrimaryDark = kColorPurpleDark;
  static const kColorPrimaryLight = kColorPurpleLight;

  static const kColorBlue = Color(0xFF0091EA);
  static const kColorBlueDark = Color(0xFF2495E4);

  static Color black25 = Colors.black.withAlpha(64);
  static Color black80 = Colors.black.withAlpha(230);
  static Color blackOpacity4 = Colors.black.withOpacity(0.4);
  static Color blackOpacity5 = Colors.black.withOpacity(0.5);

  static const Color gray = Color(0xff828282);
  static const Color dustyGray = Color(0xff999999);
  static const Color wildSand = Color(0xfff5f5f5);
  static const Color silver = Color.fromRGBO(0, 0, 0, 0.4);
  static const Color athensGray = Color(0xfff4f4f5);
  static const Color disabledTextColor = Colors.grey;

  static const Color backgroundColor = Color(0xffffffff);
  static const Color deepCerulean = Color(0xff203858);

  static const shimmerBaseColor = Color(0xFFE0E0E0);
  static const shimmerHighlightColor = Color(0xFFEEEEEE);

  static const cardShadowColor = Color(0xFFd3d1d1);
  static const headerTextColor = Color(0xFFe5e5e5);
  static const dividerColor = Color(0xFF575c69);
  static const borderColor = Color(0xFFd3d1d1);

  static const Color shadowColor = Color(0x190a1f44);
}
