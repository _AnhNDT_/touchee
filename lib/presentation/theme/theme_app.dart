import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:touchee/presentation/theme/theme_color.dart';
import 'package:touchee/presentation/theme/theme_by_viewport.dart';

class AppTheme extends ThemeByViewPort {
  static const Color primaryColor = AppColor.kPrimaryColor;
  static const Color primaryColorBare = AppColor.kColorBlueBare;
  static const Color accentColor = AppColor.kAccentColor;
  static const Color disabledColor = AppColor.kColorGrayLight;
  static const Color hintColor = AppColor.kColorGray;
  static const Color cardBorderColor = AppColor.kColorGrayLight;
  static const Color placeholderColor = AppColor.kColorGrayLighter;
  static const Color placeholderHighlightColor = AppColor.kColorGrayBare;

  ThemeData build(BuildContext context, {Brightness brightness}) {
    final themeData = _buildBaseAppTheme(context, brightness: brightness);
    return extendByViewPortSize(themeData, context);
  }

  ThemeData _buildBaseAppTheme(BuildContext context,
      {Brightness brightness = Brightness.light}) {
    final textColor =
        (brightness == Brightness.light) ? Colors.black : Colors.white;
    final base = Theme.of(context).copyWith(
      textTheme: Theme.of(context).textTheme.apply(
          fontFamily: 'GoogleSans',
          bodyColor: AppColor.kColorBlack,
          decorationColor: AppColor.kColorGrayDark,
          displayColor: AppColor.kColorBlack),
    );

    return base.copyWith(
      brightness: brightness,
      platform: defaultTargetPlatform == TargetPlatform.iOS
          ? TargetPlatform.iOS
          : TargetPlatform.android,
      accentColor: accentColor,
      accentColorBrightness: Brightness.dark,
      scaffoldBackgroundColor: brightness == Brightness.light
          ? AppColor.kColorWhite
          : AppColor.kColorBlack,
      backgroundColor: brightness == Brightness.light
          ? AppColor.kColorWhite
          : AppColor.kColorBlack,
      primaryColor: primaryColor,
      primaryColorDark: AppColor.kColorPrimaryDark,
      primaryColorLight: AppColor.kColorPrimaryLight,
      errorColor: AppColor.kColorRed,
      toggleableActiveColor: AppColor.kColorOrange,
      disabledColor: AppColor.kColorGrayLight,
      unselectedWidgetColor: AppColor.kColorGray,
      highlightColor: AppColor.kColorTransparent,
      textSelectionColor: AppColor.kColorYellowLightA1,
      bottomAppBarColor: AppColor.kColorWhite,
      hintColor: AppColor.kColorGray,
      splashColor: AppColor.kColorGrayBareA1,
      cardColor: AppColor.kColorWhite,
      dividerColor: AppColor.kColorGrayLight,
      cursorColor: primaryColor,
      buttonColor: AppColor.kColorBlueDark,
      indicatorColor: AppColor.kColorGrayLight,
      inputDecorationTheme: base.inputDecorationTheme.copyWith(
        border: const UnderlineInputBorder(
          borderSide: BorderSide(color: AppColor.kColorBlackLight, width: 1),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(
            color: primaryColor,
            width: 1,
          ),
        ),
      ),
      iconTheme: base.iconTheme.copyWith(
        color: AppColor.kColorBlack,
        size: 32,
      ),
      accentIconTheme: base.accentIconTheme.copyWith(
        color: primaryColor,
        size: 32,
      ),
      primaryIconTheme: base.primaryIconTheme.copyWith(
        color: AppColor.kColorWhite,
        size: 32,
      ),
      accentTextTheme: base.accentTextTheme.copyWith(
        button: base.accentTextTheme.button.copyWith(color: primaryColor),
        subtitle1: base.accentTextTheme.button.copyWith(color: primaryColor),
        subtitle2: base.accentTextTheme.button.copyWith(color: primaryColor),
      ),
      textTheme: base.textTheme.copyWith(
          headline1: base.textTheme.headline1.copyWith(
            color: textColor,
            fontWeight: FontWeight.w500,
            fontSize: 20,
          ),
          headline2: base.textTheme.headline2.copyWith(
            color: textColor,
            fontWeight: FontWeight.w700,
            fontSize: 34.0,
          ),
          headline3: base.textTheme.headline3.copyWith(
            color: textColor,
            fontWeight: FontWeight.w500,
            fontSize: 34.0,
          ),
          headline4: base.textTheme.headline4.copyWith(
            color: textColor,
            fontWeight: FontWeight.w700,
            fontSize: 28.0,
          ),
          headline5: base.textTheme.headline5.copyWith(
            color: textColor,
            fontWeight: FontWeight.w500,
            fontSize: 25.0,
          ),
          headline6: base.textTheme.headline6.copyWith(
            color: textColor,
            fontWeight: FontWeight.w500,
            fontSize: 22.0,
          ),
          subtitle1: base.textTheme.subtitle1.copyWith(
            color: textColor,
            fontWeight: FontWeight.normal,
            fontSize: 18.0,
          ),
          subtitle2: base.textTheme.subtitle2.copyWith(
            color: textColor,
            fontWeight: FontWeight.w400,
            fontSize: 16.0,
          ),
          bodyText1: base.textTheme.bodyText1.copyWith(
            color: textColor,
            fontWeight: FontWeight.w400,
            fontSize: 16,
          ),
          bodyText2: base.textTheme.bodyText1.copyWith(
            color: textColor,
            fontWeight: FontWeight.w400,
            fontSize: 18,
          ),
          caption: base.textTheme.caption.copyWith(
            color: textColor,
            fontWeight: FontWeight.w400,
            fontSize: 14.0,
          ),
          button: base.textTheme.button.copyWith(
            color: textColor,
            fontWeight: FontWeight.w500,
            fontSize: 14,
          ),
          overline: base.textTheme.overline.copyWith(
              color: textColor,
              fontWeight: FontWeight.w400,
              fontSize: 12.0,
              letterSpacing: 0.5)),
      primaryTextTheme: base.primaryTextTheme.copyWith(
          headline1: base.primaryTextTheme.headline1.copyWith(
              color: AppColor.kColorWhite,
              fontWeight: FontWeight.w700,
              fontSize: 34.0),
          headline2: base.primaryTextTheme.headline2.copyWith(
              color: AppColor.kColorWhite,
              fontWeight: FontWeight.w500,
              fontSize: 34.0),
          headline3: base.primaryTextTheme.headline3.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w700,
            fontSize: 28.0,
          ),
          headline4: base.primaryTextTheme.headline4.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w500,
            fontSize: 25.0,
          ),
          headline5: base.primaryTextTheme.headline5.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w500,
            fontSize: 22.0,
          ),
          headline6: base.primaryTextTheme.headline6.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w700,
            fontSize: 25.0,
          ),
          subtitle1: base.primaryTextTheme.subtitle1.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w400,
            fontSize: 16.0,
          ),
          bodyText1: base.primaryTextTheme.bodyText1.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w500,
            fontSize: 15.5,
          ),
          bodyText2: base.primaryTextTheme.bodyText2.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w400,
            fontSize: 15.5,
          ),
          caption: base.primaryTextTheme.caption.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w400,
            fontSize: 14.0,
          ),
          button: base.primaryTextTheme.button.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w500,
            fontSize: 15.5,
          ),
          subtitle2: base.primaryTextTheme.subtitle2.copyWith(
            color: AppColor.kColorWhite,
            fontWeight: FontWeight.w500,
            fontSize: 20,
          ),
          overline: base.primaryTextTheme.overline.copyWith(
              color: AppColor.kColorWhite,
              fontWeight: FontWeight.w400,
              fontSize: 12.0,
              letterSpacing: 0.5)),
      chipTheme: base.chipTheme.copyWith(
          backgroundColor: AppColor.kColorGrayBare,
          disabledColor: AppColor.kColorGrayLighter,
          selectedColor: AppColor.kColorGrayDark,
          secondarySelectedColor: AppColor.kColorOrangeDark,
          labelPadding: const EdgeInsets.symmetric(horizontal: 12.0),
          padding: const EdgeInsets.all(0.0),
          shape: const StadiumBorder(),
          labelStyle: const TextStyle(
            color: AppColor.kColorGrayDark,
            fontSize: 15.5,
          ),
          secondaryLabelStyle: const TextStyle(
            color: AppColor.kColorGrayDark,
          ),
          brightness: Brightness.dark),
      buttonTheme: ButtonThemeData(
          highlightColor: AppColor.kColorTransparent,
          splashColor: AppColor.kColorGrayBareA1,
          colorScheme: base.buttonTheme.colorScheme.copyWith(
              primary: base.primaryColor,
              primaryVariant: base.primaryColorDark,
              secondary: AppColor.kColorOrange,
              secondaryVariant: AppColor.kColorOrangeDark,
              surface: base.primaryColor,
              onSurface: AppColor.kColorWhite,
              brightness: brightness,
              onSecondary: AppColor.kColorWhite,
              onPrimary: AppColor.kColorWhite,
              onError: AppColor.kColorWhite,
              onBackground: AppColor.kColorBlack,
              error: base.errorColor,
              background: AppColor.kColorGrayLight)),
      colorScheme: base.colorScheme.copyWith(
          onPrimary: AppColor.kColorWhite,
          background: brightness == Brightness.light
              ? AppColor.kColorWhite
              : AppColor.kColorBlack,
          brightness: brightness,
          error: AppColor.kColorRed,
          onBackground: brightness == Brightness.light
              ? AppColor.kColorBlack
              : AppColor.kColorWhite,
          onError: AppColor.kColorWhite,
          onSecondary: AppColor.kColorLightGray,
          primary: AppColor.kColorBlue,
          primaryVariant: AppColor.kColorBlueDark,
          secondary: AppColor.kColorOrange,
          secondaryVariant: AppColor.kColorOrangeDark,
          surface: AppColor.kColorWhite,
          onSurface: AppColor.kColorGrayDark),
      dialogTheme: base.dialogTheme.copyWith(elevation: 0),
    );
  }
}
