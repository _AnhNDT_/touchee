import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:touchee/common/constants/ui_constants.dart';

abstract class ThemeByViewPort {
  ThemeData _scaleTheme(ThemeData base, double scale) {
    final theme = base.copyWith(
      textTheme: base.textTheme.copyWith(
        headline1: base.textTheme.headline1
            .copyWith(fontSize: base.textTheme.headline1.fontSize * scale),
        headline2: base.textTheme.headline2
            .copyWith(fontSize: base.textTheme.headline2.fontSize * scale),
        headline3: base.textTheme.headline3
            .copyWith(fontSize: base.textTheme.headline3.fontSize * scale),
        headline4: base.textTheme.headline4
            .copyWith(fontSize: base.textTheme.headline4.fontSize * scale),
        headline5: base.textTheme.headline5
            .copyWith(fontSize: base.textTheme.headline5.fontSize * scale),
        headline6: base.textTheme.headline6
            .copyWith(fontSize: base.textTheme.headline6.fontSize * scale),
        subtitle1: base.textTheme.subtitle1
            .copyWith(fontSize: base.textTheme.subtitle1.fontSize * scale),
        bodyText1: base.textTheme.bodyText1
            .copyWith(fontSize: base.textTheme.bodyText1.fontSize * scale),
        bodyText2: base.textTheme.bodyText2
            .copyWith(fontSize: base.textTheme.bodyText2.fontSize * scale),
        caption: base.textTheme.caption
            .copyWith(fontSize: base.textTheme.caption.fontSize * scale),
        button: base.textTheme.button
            .copyWith(fontSize: base.textTheme.button.fontSize * scale),
        subtitle2: base.textTheme.subtitle2
            .copyWith(fontSize: base.textTheme.subtitle2.fontSize * scale),
        overline: base.textTheme.overline
            .copyWith(fontSize: base.textTheme.overline.fontSize * scale),
      ),
      primaryTextTheme: base.primaryTextTheme.copyWith(
        headline1: base.primaryTextTheme.headline1.copyWith(
            fontSize: base.primaryTextTheme.headline1.fontSize * scale),
        headline2: base.primaryTextTheme.headline2.copyWith(
            fontSize: base.primaryTextTheme.headline2.fontSize * scale),
        headline3: base.primaryTextTheme.headline3.copyWith(
            fontSize: base.primaryTextTheme.headline3.fontSize * scale),
        headline4: base.primaryTextTheme.headline4.copyWith(
            fontSize: base.primaryTextTheme.headline4.fontSize * scale),
        headline5: base.primaryTextTheme.headline5.copyWith(
            fontSize: base.primaryTextTheme.headline5.fontSize * scale),
        headline6: base.primaryTextTheme.headline6.copyWith(
            fontSize: base.primaryTextTheme.headline6.fontSize * scale),
        subtitle1: base.primaryTextTheme.subtitle1.copyWith(
            fontSize: base.primaryTextTheme.subtitle1.fontSize * scale),
        bodyText1: base.primaryTextTheme.bodyText1.copyWith(
            fontSize: base.primaryTextTheme.bodyText1.fontSize * scale),
        bodyText2: base.primaryTextTheme.bodyText2.copyWith(
            fontSize: base.primaryTextTheme.bodyText2.fontSize * scale),
        caption: base.primaryTextTheme.caption
            .copyWith(fontSize: base.primaryTextTheme.caption.fontSize * scale),
        button: base.primaryTextTheme.button
            .copyWith(fontSize: base.primaryTextTheme.button.fontSize * scale),
        subtitle2: base.primaryTextTheme.subtitle2.copyWith(
            fontSize: base.primaryTextTheme.subtitle2.fontSize * scale),
        overline: base.primaryTextTheme.overline.copyWith(
            fontSize: base.primaryTextTheme.overline.fontSize * scale),
      ),
      iconTheme: base.iconTheme.copyWith(size: 23 * scale),
      accentIconTheme: base.accentIconTheme.copyWith(size: 23 * scale),
      primaryIconTheme: base.primaryIconTheme.copyWith(size: 23 * scale),
    );

    return theme;
  }

  ThemeData extendTablet(BuildContext context, {ThemeData themeData}) {
    return themeData ?? Theme.of(context);
  }

  ThemeData extendByViewPortSize(ThemeData themeData, BuildContext context) {
    ThemeData _extThemeData;
    final _viewPortSize = window.physicalSize.width / window.devicePixelRatio;
    final _base = themeData ?? Theme.of(context);

    if (_viewPortSize <= UiConstants.maxMobileViewportSize) {
      _extThemeData = _base;
      // ignore: invariant_booleans
    } else if (_viewPortSize > UiConstants.maxMobileViewportSize &&
        _viewPortSize <= UiConstants.maxViewportSize) {
      _extThemeData = _scaleTheme(
          _base,
          math.min(_viewPortSize / UiConstants.baseViewPortSize,
              UiConstants.maxScale));
    } else {
      _extThemeData = _scaleTheme(
          _base,
          math.min(_viewPortSize / UiConstants.baseViewPortSize,
              UiConstants.tabletMaxScale));
    }

    return _extThemeData;
  }
}
