import 'package:touchee/common/event_bus/abstract_event.dart';

class UnauthenticatedEmitterEvent extends Event {
  UnauthenticatedEmitterEvent();
}
