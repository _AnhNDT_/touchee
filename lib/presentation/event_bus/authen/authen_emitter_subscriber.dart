import 'package:flutter/material.dart';

import 'package:touchee/common/event_bus/subscriber.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_bloc.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_event.dart';
import 'package:touchee/presentation/event_bus/authen/authen_emitter_events.dart';

class AuthenEmitterSubscriber extends Subscriber<UnauthenticatedEmitterEvent> {
  final AuthenticationBloc authenticationBloc;

  AuthenEmitterSubscriber({@required this.authenticationBloc});

  @override
  void onEvent(UnauthenticatedEmitterEvent event) {
    authenticationBloc?.add(LoggedOutEvent());
  }
}
