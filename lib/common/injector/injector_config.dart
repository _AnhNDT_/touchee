import 'package:kiwi/kiwi.dart';

import 'package:touchee/domain/usecases/config_usecase.dart';
import 'package:touchee/presentation/common_bloc/config_bloc/config_bloc.dart';
import 'package:touchee/common/network/http/touchee_client.dart';
import 'package:touchee/common/network/network_info.dart';
import 'package:touchee/data/repositories/config_repository_impl.dart';
import 'package:touchee/presentation/common_bloc/language_bloc/language_bloc.dart';
import 'package:touchee/data/datasources/remote/config_remote_datasource.dart';
import 'package:touchee/data/datasources/local_data/shared_preference_local_datasource.dart';
import 'package:touchee/data/datasources/remote/user_remote_datasource.dart';
import 'package:touchee/data/repositories/user_repository_impl.dart';
import 'package:touchee/domain/usecases/user_usecase.dart';
import 'package:touchee/presentation/common_bloc/authen_bloc/authen_bloc.dart';
import 'package:touchee/presentation/common_bloc/loader_bloc/loader_bloc.dart';

part 'injector_config.g.dart';

abstract class InjectorConfig {
  static KiwiContainer container;

  static void setup() {
    container = KiwiContainer();
    final injector = _$InjectorConfig();
    // ignore: cascade_invocations
    injector._configure();
  }

  // ignore: type_annotate_public_apis
  static final resolve = container.resolve;

  void _configure() {
    _configureInsuranceModule();
  }

  void _configureInsuranceModule() {
    _configureBlocs();
    _configureUseCases();
    _configureRepositories();
    _configureDataSources();
    _configureLocalDataSources();
    _configureExternal();
    _configureCommon();
    _configureUtils();
  }

  @Register.factory(ConfigBloc)
  @Register.singleton(LoaderBloc)
  @Register.singleton(LanguageBloc)
  @Register.singleton(AuthenticationBloc)
  void _configureBlocs();

  @Register.factory(ConfigUseCase)
  @Register.factory(UserUseCase)
  void _configureUseCases();

  @Register.factory(ConfigRepositoryImpl)
  @Register.factory(UserRepositoryImpl)
  void _configureRepositories();

  @Register.factory(ConfigRemoteDataSource)
  @Register.factory(UserRemoteDataSource)
  void _configureDataSources();

  @Register.singleton(SharedPreferenceLocalDataSource)
  void _configureLocalDataSources();

  @Register.singleton(ToucheeClient)
  void _configureExternal();

  @Register.factory(NetworkInfoImpl)
  void _configureCommon();

  void _configureUtils();
}
