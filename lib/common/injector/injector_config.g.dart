// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'injector_config.dart';

// **************************************************************************
// KiwiInjectorGenerator
// **************************************************************************

class _$InjectorConfig extends InjectorConfig {
  void _configureBlocs() {
    KiwiContainer()
      ..registerSingleton((c) => LanguageBloc())
      ..registerSingleton((c) => LoaderBloc())
      ..registerFactory((c) => ConfigBloc(configUseCase: c<ConfigUseCase>()))
      ..registerSingleton((c) => AuthenticationBloc(
            userUseCase: c<UserUseCase>(),
            loaderBloc: c<LoaderBloc>(),
          ));
  }

  void _configureUseCases() {
    KiwiContainer()
      ..registerFactory(
          (c) => ConfigUseCase(configRepository: c<ConfigRepositoryImpl>()))
      ..registerFactory(
          (c) => UserUseCase(userRepository: c<UserRepositoryImpl>()));
  }

  void _configureRepositories() {
    KiwiContainer()
      ..registerFactory((c) => ConfigRepositoryImpl(
            c<NetworkInfoImpl>(),
            c<ConfigRemoteDataSource>(),
          ))
      ..registerFactory((c) => UserRepositoryImpl(
            c<NetworkInfoImpl>(),
            c<UserRemoteDataSource>(),
            c<SharedPreferenceLocalDataSource>(),
          ));
  }

  void _configureDataSources() {
    KiwiContainer()
      ..registerFactory(
          (c) => ConfigRemoteDataSource(toucheeClient: c<ToucheeClient>()))
      ..registerFactory(
          (c) => UserRemoteDataSource(toucheeClient: c<ToucheeClient>()));
  }

  @override
  void _configureLocalDataSources() {
    KiwiContainer()
      ..registerSingleton((c) => SharedPreferenceLocalDataSource());
  }

  void _configureExternal() {
    final KiwiContainer container = KiwiContainer();
    container.registerSingleton((c) => ToucheeClient());
  }

  void _configureCommon() {
    final KiwiContainer container = KiwiContainer();
    container.registerFactory((c) => NetworkInfoImpl());
  }

  void _configureUtils() {}
}
