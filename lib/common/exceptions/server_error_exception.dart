import 'package:touchee/common/constants/exception_constants.dart';
import 'package:touchee/common/exceptions/server_exception.dart';

class ServerErrorException extends BaseException {
  ServerErrorException(Map<String, dynamic> error)
      : super(
          message: error['message'],
          code: ExceptionConstants.internalServerError,
          errors: <ErrorList>[],
        );
}
