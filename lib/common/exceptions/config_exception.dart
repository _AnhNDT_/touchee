class ConfigException implements Exception {
  final String error;
  final String message;

  ConfigException({this.error, this.message});
}
