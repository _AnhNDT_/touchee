import 'package:touchee/domain/entities/base_result_entity.dart';

class DataException implements Exception {
  BaseResultEntity data;

  DataException({this.data});
}
