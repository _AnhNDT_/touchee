import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:touchee/common/exceptions/authen_exception.dart';
import 'package:touchee/common/exceptions/login_exception.dart';
import 'package:touchee/data/models/base_result_model.dart';
import 'package:touchee/common/utils/remote_utils.dart';
import 'package:touchee/common/network/http_constants.dart';
import 'package:touchee/common/exceptions/server_error_exception.dart';

import 'package:http/http.dart';

class HttpUtil {
  static dynamic encodeRequestBody(dynamic data, String contentType) {
    return contentType == HttpConstants.jsonContentType
        ? utf8.encode(json.encode(data))
        : data;
  }

  static dynamic getResponse(Response response) {
    debugPrint('>>>>>>> ${response.body}');
    switch (response.statusCode) {
      case 200:
        final baseResult = _getSuccessResponse(response);
        switch (baseResult?.code ?? 0) {
          case 200:
            return baseResult.data;
          case 401:
            throw AuthenException();
          default:
            throw DataException(data: baseResult);
        }
        break;
      case 403:
        throw AuthenException();
      default:
        throw ServerErrorException(
          getErrorResult(json.decode(response.body)),
        );
    }
  }

  static BaseResultModel _getSuccessResponse(Response response) {
    return BaseResultModel.fromJson(json.decode(response.body));
  }
}
