import 'package:flutter/material.dart';

import 'package:touchee/common/internationalization.dart';

class ValidationUtils {
  static String validatePhoneNumber(BuildContext context, String _value) {
    const pattern = r'(^(?:[+0]9)?[0-9]{10}$)';
    final regExp = RegExp(pattern);
    var errorMsg = '';
    if (_value.isEmpty) {
      errorMsg =
          S.of(context).translate('validation.phoneNumber.notEmptyMessage');
    } else if (!(_value.startsWith('0') && regExp.hasMatch(_value))) {
      errorMsg =
          S.of(context).translate('validation.phoneNumber.invalidMessage');
    }

    return errorMsg;
  }

  static bool isValidEmail(String email) {
    const pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    return RegExp(pattern, caseSensitive: false).hasMatch(email);
  }

  static bool isValidIdentifyNumber(String identifyNumber) {
    const pattern = r'[a-zA-Z0-9]{8,12}$';
    return RegExp(pattern, caseSensitive: false).hasMatch(identifyNumber);
  }

  static bool isValidatePhoneNumber(String phoneNumber) {
    const pattern = r'(^(?:[+0]9)?[0-9]{10}$)';
    final regExp = RegExp(pattern);
    if (phoneNumber.isEmpty) {
      return false;
    } else if (!(phoneNumber.startsWith('0') && regExp.hasMatch(phoneNumber))) {
      return false;
    }

    return true;
  }

  /// https://stackoverflow.com/questions/12018245/regular-expression-to-validate-username
  static bool isValidUserName(String userName) {
    const pattern = r'^(?=[a-zA-Z0-9._]{4,30}$)(?!.*[_.]{2})[^_.].*[^_.]$';
    final regExp = RegExp(pattern);
    return regExp.hasMatch(userName);
  }
}
