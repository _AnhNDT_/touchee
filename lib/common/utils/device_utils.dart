import 'dart:io';
import 'dart:ui' as ui;

import 'package:device_info/device_info.dart';

class Device {
  static double devicePixelRatio = ui.window.devicePixelRatio;
  static ui.Size size = ui.window.physicalSize;
  static double width = size.width;
  static double height = size.height;
  static double screenWidth = width / devicePixelRatio;
  static double screenHeight = height / devicePixelRatio;
  static ui.Size screenSize = ui.Size(screenWidth, screenHeight);

  final bool isIos, isAndroid;
  final double paddingVerticalInsets;
  static Device _device;

  Device({
    this.isIos,
    this.isAndroid,
    this.paddingVerticalInsets,
  });

  factory Device.get() {
    if (_device != null) {
      return _device;
    }

    final isIos = Platform.isIOS;
    final isAndroid = Platform.isAndroid;
    var paddingVerticalInsets = 0.0;

    if (isIos &&
        (screenHeight == 812 ||
            screenWidth == 812 ||
            screenHeight == 896 ||
            screenWidth == 896)) {
      paddingVerticalInsets = 50.0;
    }

    return _device = Device(
      isAndroid: isAndroid,
      isIos: isIos,
      paddingVerticalInsets: paddingVerticalInsets,
    );
  }
}

Future<Map<String, dynamic>> getDeviceInfoJson() async {
  final deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) {
    final iosInfo = await deviceInfo.iosInfo;
    return {
      'device_id': iosInfo?.identifierForVendor ?? '',
      'device_name': iosInfo?.name ?? '',
      'model': iosInfo?.model ?? '',
      'is_physical': iosInfo?.isPhysicalDevice ?? '',
      'os_version': iosInfo?.systemVersion ?? '',
      'type': 'ios',
    };
  } else if (Platform.isAndroid) {
    final androidInfo = await deviceInfo.androidInfo;
    return {
      'device_id': androidInfo?.androidId ?? '',
      'device_name': androidInfo?.device ?? '',
      'model': androidInfo?.model ?? '',
      'is_physical': androidInfo?.isPhysicalDevice ?? '',
      'brand': androidInfo?.brand ?? '',
      'os_version': '${androidInfo?.version?.sdkInt ?? ''} - '
          '${androidInfo?.id ?? ''}',
      'type': 'android ${androidInfo?.version?.release ?? ''}',
    };
  } else {
    return {
      'device_id': 'UNKNOWN',
      'device_name': 'UNKNOWN',
      'type': 'UNKNOWN',
    };
  }
}
