import 'package:bot_toast/bot_toast.dart';

import 'package:touchee/presentation/widgets/toast_widget/toast_widget.dart';
import 'package:touchee/presentation/widgets/toast_widget/toast_widget_enum.dart';

class ToastUtils {
  static void show({
    String message = '',
    String translateKey = '',
    ToastEnum type = ToastEnum.info,
  }) {
    BotToast.showCustomNotification(
      animationDuration: const Duration(milliseconds: 200),
      animationReverseDuration: const Duration(milliseconds: 100),
      duration: const Duration(seconds: 2),
      toastBuilder: (cancel) {
        return ToastWidget(
          message: message,
          translateKey: translateKey,
          type: type,
        );
      },
      enableSlideOff: true,
      onlyOne: true,
      crossPage: true,
      backButtonBehavior: BackButtonBehavior.none,
    );
  }
}
