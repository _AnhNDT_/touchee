import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScreenUtils {
  factory ScreenUtils() => _instance;

  ScreenUtils._internal();

  static final ScreenUtils _instance = ScreenUtils._internal();

  double getDeviceWidthByPercent({double percent = 0.0}) {
    return ScreenUtil.screenWidth * percent;
  }

  double getDeviceHeightByPercent({double percent = 0.0}) {
    return ScreenUtil.screenHeight * percent;
  }

  double getDefaultScreenWidth() {
    return isTablet()
        ? ScreenUtil.screenWidth * .8
        : ScreenUtil.screenWidth * .95;
  }

  double getDrawerWidth() {
    return isTablet()
        ? ScreenUtil.screenWidth * .45
        : ScreenUtil.screenWidth * .8;
  }

  double getFormWidth() {
    return isTablet()
        ? ScreenUtil.screenWidth * .45
        : ScreenUtil.screenWidth * .7;
  }

  bool isTablet() {
    return ScreenUtil.screenWidth > 600;
  }

  double getExpandedButtonWidth() {
    return isTablet() ? ScreenUtil.screenWidth * .5 : double.infinity;
  }
}
