import 'package:flutter/material.dart';

import 'package:touchee/common/event_bus/subscriber.dart';
import 'package:touchee/common/event_bus/abstract_event.dart';

class EventBus {
  static final Map<dynamic, List<Subscriber<dynamic>>> _subscribers = {};

  static void subscribe<T extends Event>(Subscriber<T> subscriber) {
    _subscribers.putIfAbsent(T, () {
      return [];
    });
    _subscribers[T].add(subscriber);
  }

  static void unsubscribe<T extends Event>(Subscriber<T> subscriber) {
    // ignore: omit_local_variable_types
    final List<Subscriber<dynamic>> list = _subscribers[T] ?? [];

    if (list.isEmpty) {
      debugPrint('>>>>>>> EventBus Queue is empty, no subscriber.');
    }

    if (list.remove(subscriber)) {
      if (list.isEmpty) {
        _subscribers.remove(T);
      }
    }
  }

  static void emit<T extends Event>(T event) {
    // ignore: omit_local_variable_types
    final List<Subscriber<dynamic>> queue = _subscribers[T] ?? [];

    if (queue.isEmpty) {
      debugPrint('>>>>>>> EventBus Failed to fire the event. '
          'Queue is empty, no subscriber');
    }
    for (final subscriber in queue) {
      try {
        subscriber.onEvent(event);
      } catch (error) {
        debugPrint('>>>>>>> Receive Event ${error.toString()}');
      }
    }
  }

  static void reset() {
    _subscribers.clear();
  }

  static Map<dynamic, List<Subscriber<dynamic>>> get getQueue => _subscribers;
}
