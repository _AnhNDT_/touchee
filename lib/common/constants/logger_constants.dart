import 'package:logger/logger.dart';

import 'package:touchee/common/utils/logger_utils.dart';

LoggerUtil toucheeLog = LoggerUtil(
  printer: PrettyPrinter(
    methodCount: 0,
    lineLength: 150,
    printTime: true,
    printEmojis: false,
  ),
  level: LoggerUtil.logLevel(),
);
