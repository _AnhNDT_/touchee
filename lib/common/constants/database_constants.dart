class DatabaseConstant {
  /// When you change or delete a value,
  /// the change is written to the end of the box file.
  /// This leads sooner or later to a growing box file.
  /// Invoking compaction manually after threshold value
  static int compactionLimit = 20;
}
