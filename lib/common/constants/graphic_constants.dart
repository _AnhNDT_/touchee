class GraphicConstants {
  static const icLogo = '././res/graphics/ic_no_signal.png';

  static const icNoData = '././res/graphics/ic_no_data.png';
  static const icNoSignal = '././res/graphics/ic_no_signal.png';
  static const icError = '././res/graphics/ic_error.png';
  static const icNotGrantPermission =
      '././res/graphics/ic_not_grant_permission.png';
}
