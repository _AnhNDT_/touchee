class UiConstants {
  /// Base viewport size where things are scaled up from
  static const baseViewPortSize = 375;

  /// Max viewport width for mobile
  static const maxMobileViewportSize = 414.0;

  /// Max supported viewport width
  static const maxViewportSize = 480.0;

  /// Max scale for all viewports <= 480
  ///
  /// Currently, scale everything up to 375
  static const maxScale = 1.15;

  /// Max scale for viewports > 480 (i.e. Tablets)
  static const tabletMaxScale = 1.2;
}
