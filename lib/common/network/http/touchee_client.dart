import 'package:touchee/common/configurations/configurations.dart';

import 'http_client.dart';

class ToucheeClient extends HttpClient {
  ToucheeClient()
      : super(
          host: Configuration.host,
          header: getAuthenticationHeader(),
        );

  static Map<String, String> getAuthenticationHeader() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'x-api-key': 'e8839f6a-0c6e-11ea-8d71-362b9e155667',
      'Device-Info': Configuration.appInfo.toString(),
    };
  }

  void updateAuthenticatedTokenHeader({String token}) {
    header = {
      ...header,
      'Authorization': 'Bearer $token',
    };
  }
}
