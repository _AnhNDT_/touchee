import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:meta/meta.dart';
import 'package:http/http.dart';

import 'package:touchee/common/utils/http_util.dart';
import 'package:touchee/common/utils/common_utils.dart';
import 'package:touchee/common/network/http_constants.dart';
import 'package:touchee/common/exceptions/config_exception.dart';
import 'package:touchee/common/configurations/configurations.dart';

class HttpClient {
  Client _client;
  String host;
  Map<String, String> header;

  HttpClient({@required this.host, @required this.header}) {
    _client = Client();
  }

  String _getParsedUrl(String path) {
    return Uri.encodeFull('$host$path');
  }

  dynamic get(
    String path, {
    Map<String, dynamic> params,
    bool clearHttpCache = false,
  }) async {
    final _attachParamMap = <String, dynamic>{};

    if (clearHttpCache) {
      _attachParamMap['_t'] = '${DateTime.now().millisecondsSinceEpoch}';
    }

    _attachParamMap.addAll(params ?? <String, dynamic>{});
    if (_attachParamMap?.isNotEmpty ?? false) {
      path = '${_getParsedUrl(path)}?${await _buildUrl(_attachParamMap)}';
    }

    debugPrint('>>>>>>> [HEADER] ${header.toString()}');
    debugPrint('>>>>>>> [GET] $path');

    final response = await _client
        .get(
          path,
          headers: header,
        )
        .timeout(
          const Duration(seconds: 30),
          onTimeout: () => _onRequestTimeout(path),
        );

    return HttpUtil.getResponse(response);
  }

  dynamic post(String path, dynamic data,
      {Map<String, dynamic> overrideHeader}) async {
    final Map<String, String> requestHeader = overrideHeader ?? header;

    debugPrint('[POST] ${_getParsedUrl(path)}');
    debugPrint('[HEADER] ${header.toString()}');
    debugPrint('[DATA] ${data.toString()}');

    final response = await _client
        .post(
          _getParsedUrl(path),
          body: HttpUtil.encodeRequestBody(
              json.encode(data), requestHeader[HttpConstants.contentType]),
          headers: requestHeader,
        )
        .timeout(
          const Duration(seconds: 30),
          onTimeout: () => _onRequestTimeout(path),
        );

    return HttpUtil.getResponse(response);
  }

  Future<String> _buildUrl(Map<String, dynamic> params) async {
    params ??= <String, dynamic>{};

    final _appInfo = Configuration.appInfo;

    if (_appInfo == null) {
      throw ConfigException(
        error: 'ConfigException',
        message: 'Can not get application info',
      );
    }

    params
      ..putIfAbsent('lang', () => 'en')
      ..putIfAbsent('appVersion', () => _appInfo?.buildNumber)
      ..putIfAbsent('os', () => _appInfo?.platform)
      ..putIfAbsent(
          'timestamp', () => '${DateTime.now().millisecondsSinceEpoch}')
      ..putIfAbsent('sign', () => generateMd5(_buildSignature(params)));

    return params.keys.map((k) {
      if (k is Map) {
        return '$k=${Uri.encodeComponent(params[k])}';
      }
      return '$k=${params[k]}';
    }).join('&');
  }

  String _buildSignature(Map<String, dynamic> params) {
    if (params == null) {
      return '';
    }

    final items = <String>[];
    params.forEach((key, value) {
      items.add('$key=${Uri.encodeComponent(value?.toString())}');
    });
    items.sort();

    final _result = items.reduce((value, element) => '$value,$element');
    return '$_result,sign=${Configuration.secretKey}';
  }

  Future<Response> _onRequestTimeout(String path) async {
    debugPrint('>>>>>>> Request timeout: $path');
    return null;
  }
}
