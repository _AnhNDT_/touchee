import 'dart:io';

import 'package:flutter/foundation.dart';

import 'package:package_info/package_info.dart';

import 'package:touchee/common/utils/device_utils.dart';
import 'package:touchee/common/configurations/config_constants.dart';
import 'package:touchee/domain/entities/application_info_entity.dart';

class Configuration {
  factory Configuration() => _instance;

  Configuration._internal();

  static final Configuration _instance = Configuration._internal();

  static String _host = DefaultConfig.host;
  static String _secretKey = DefaultConfig.secretKey;
  static ApplicationInfoEntity _appInfo;

  Future<void> initConfiguration({
    @required FlavorEnum flavor,
    @required Map<String, dynamic> configs,
  }) async {
    _appInfo = await _getApplicationInfo();

    if (flavor == FlavorEnum.pro) {
      debugPrint = (String message, {int wrapWidth}) {};
    }

    final envConfig = configs[describeEnum(flavor)];
    _host = envConfig['host'] ?? DefaultConfig.host;
    _secretKey = envConfig['key'] ?? DefaultConfig.secretKey;
  }

  Future<ApplicationInfoEntity> _getApplicationInfo() async {
    final info = await PackageInfo.fromPlatform();
    final extendInfo = await getDeviceInfoJson();
    return ApplicationInfoEntity(
      platform: Platform.operatingSystem,
      appName: info?.appName,
      buildNumber: info?.buildNumber,
      packageName: info?.packageName,
      version: info?.version,
      extendInfo: extendInfo,
    );
  }

  static String get host => _host;

  static String get secretKey => _secretKey;

  static ApplicationInfoEntity get appInfo => _appInfo;
}
